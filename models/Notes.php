<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notes".
 *
 * @property int $id
 * @property string $title
 * @property string $message
 * @property string $creator
 * @property string $date_created
 * @property string $date_updated
 */
class Notes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'message', 'creator'], 'required'],
            [['message'], 'string'],
            [['date_created', 'date_updated'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['creator'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'message' => 'Message',
            'creator' => 'Creator',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
        ];
    }
}
